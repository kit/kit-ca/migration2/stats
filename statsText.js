/*Data sample:
{ 
      "key" : "North America" , 
      "values" : [ [ 1025409600000 , 23.041422681023] , [ 1028088000000 , 19.854291255832],
       [ 1030766400000 , 21.02286281168], 
       [ 1033358400000 , 22.093608385173],
       [ 1036040400000 , 25.108079299458],
       [ 1038632400000 , 26.982389242348]
       ...

*/

date = new Date().getTime();

function checkDate(myDate) {
  return myDate[0] > date;
}

d3.json('stats-past-hires.json', function(data) {
  tbl = document.createElement('table');
  tbl.style.border = '1px solid black';
  tbl.style.borderCollapse = 'collapse';

  today = data[0]['values'].findIndex(checkDate);
  if (today < 0) {
    today = data[0]['values'].length - 1;
  }

  tr = tbl.insertRow();
  countTotal = 0;
  for (var dataset = 0; dataset < data.length; dataset += 2) {
    td = tr.insertCell();
    td.style.padding = '10px';
    td.style.paddingLeft = '20px';
    td.style.textAlign = 'left';
    td.appendChild(document.createTextNode(data[dataset]['key'] + ':'));
    td = tr.insertCell();
    td.style.paddingRight = '20px';
    td.style.textAlign = 'right';
    td.appendChild(document.createTextNode(data[dataset]['values'][today][1]));
    countTotal += data[dataset]['values'][today][1];
  }
  td = tr.insertCell();
  td.style.paddingLeft = '20px';
  td.style.textAlign = 'left';
  td.style.borderLeft = '1px solid black';
  td.appendChild(document.createTextNode('Nutzerzertifikate insgesamt: '));
  td = tr.insertCell();
  td.style.padding = '10px';
  td.style.paddingRight = '20px';
  td.style.textAlign = 'right';
  td.appendChild(document.createTextNode(countTotal));

  tr = tbl.insertRow();
  countTotal = 0;
  for (var dataset = 1; dataset < data.length; dataset += 2) {
    td = tr.insertCell();
    td.style.padding = '10px';
    td.style.paddingLeft = '20px';
    td.style.textAlign = 'left';
    td.appendChild(document.createTextNode(data[dataset]['key'] + ':'));
    td = tr.insertCell();
    td.style.paddingRight = '20px';
    td.style.textAlign = 'right';
    td.appendChild(document.createTextNode(data[dataset]['values'][today][1]));
    countTotal += data[dataset]['values'][today][1];
  }
  td = tr.insertCell();
  td.style.paddingLeft = '20px';
  td.style.textAlign = 'left';
  td.style.borderLeft = '1px solid black';
  td.appendChild(document.createTextNode('Serverzertifikate insgesamt:'));
  td = tr.insertCell();
  td.style.padding = '10px';
  td.style.paddingRight = '20px';
  td.style.textAlign = 'right';
  td.appendChild(document.createTextNode(countTotal));

  tr = tbl.insertRow();
  countTotal = 0;
  for (var dataset = 0; dataset < data.length; dataset += 2) {
    td = tr.insertCell();
    td.style.padding = '10px';
    td.style.paddingLeft = '20px';
    td.style.align = 'left';
    td.style.borderTop = '1px solid black';
    td.appendChild(document.createTextNode('Insgesamt:'));
    td = tr.insertCell();
    td.style.paddingRight = '20px';
    td.style.align = 'right';
    td.style.borderTop = '1px solid black';
    td.appendChild(document.createTextNode((data[dataset]['values'][today][1]+data[dataset+1]['values'][today][1])));
    countTotal += (data[dataset]['values'][today][1]+data[dataset+1]['values'][today][1]);
  }
  td = tr.insertCell();
  td.style.paddingLeft = '20px';
  td.style.align = 'left';
  td.style.borderTop = '1px solid black';
  td.style.borderLeft = '1px solid black';
  td.appendChild(document.createTextNode('Alle Zertifikate insgesamt: '));
  td = tr.insertCell();
  td.style.padding = '10px';
  td.style.paddingRight = '20px';
  td.style.align = 'right';
  td.style.borderTop = '1px solid black';
  td.appendChild(document.createTextNode(countTotal));

  document.getElementById('current').appendChild(tbl);
  document.getElementById('current').appendChild(document.createTextNode(d3.time.format('Stand: %Y-%0m-%0dT%H:%M:%S%Z')(new Date(data[0]['lastUpdate']))))
});
