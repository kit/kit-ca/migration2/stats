/*Data sample:
{ 
      "key" : "North America" , 
      "values" : [ [ 1025409600000 , 23.041422681023] , [ 1028088000000 , 19.854291255832],
       [ 1030766400000 , 21.02286281168], 
       [ 1033358400000 , 22.093608385173],
       [ 1036040400000 , 25.108079299458],
       [ 1038632400000 , 26.982389242348]
       ...

*/

d3.json('stats.json', function(data) {
  nv.addGraph(function() {
    var chart = nv.models.stackedAreaChart()
                  .margin({right: 100})
                  .x(function(d) { return d[0] })   //We can modify the data accessor functions...
                  .y(function(d) { return d[1] })   //...in case your data is formatted differently.
                  .useInteractiveGuideline(true)    //Tooltips which show all data points. Very nice!
//                  .rightAlignYAxis(true)      //Let's move the y-axis to the right side.
                  .showControls(true)       //Allow user to choose 'Stacked', 'Stream', 'Expanded' mode.
                  .clipEdge(true);

    //Format x-axis labels with custom function.
    chart.xAxis
        .tickFormat(function(d) { 
          return d3.time.format('%Y-%0m-%0d')(new Date(d)) 
    });

    chart.yAxis
        .tickFormat(d3.format(',i'));

    d3.select('#area svg')
      .datum(data)
      .call(chart);

    // Get now timestamp
    // position will be updated each time
    var date_now = (new Date()).getTime();

    // If we want a fixed position
    // var date_now = (new Date("2015/03/18")).getTime();

    // Get position of now date on xAxis thanks to scale func
    var date_now_xpos = chart.xAxis.scale()(date_now);

    // When nvd3 draw chart it append a rectangle of the inner
    // size of our chart (without label axis or title)
    // let's get it to know height and width of our rectangle
    var rect_background = document.getElementsByClassName("nvd3")[0].firstChild.firstChild;

    // draw a background rectangle to indicate the future
    // Here we will append new rectangle to ".nv-groups"
    // this allow to avoid to break nvd3 hover fearure on chart
    d3.select('.nv-groups').append("rect")
      .attr("x", date_now_xpos) // start rectangle on the good position
      .attr("y", 0) // no vertical translate
      .attr("width", rect_background.getAttribute('width') - date_now_xpos) // correct size
      .attr("height", rect_background.getAttribute('height')) // full height
      .attr("fill", "rgba(200, 200, 200, 0.5)"); // transparency color to see grid

    nv.utils.windowResize(chart.update);

    return chart;
  });
});
